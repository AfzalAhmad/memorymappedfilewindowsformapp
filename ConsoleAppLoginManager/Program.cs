﻿using MemoryMapped.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppLoginManager
{
    class Program
    {
        static void Main(string[] args)
        {
            SharedMemory<Message> shmem =
             new SharedMemory<Message>("ShmemTest", 32);

            if (!shmem.Open()) return;

            Message data = new Message();

            // Read from shared memory
            data = shmem.Data;
            Console.WriteLine("{0}",
                data.loggedIn);

            // Change some data
            data.loggedIn = true;
            

            // Write back to shared memory
            shmem.Data = data;

            // Press any key to exit
            Console.ReadKey();

            // Close shared memory
            shmem.Close();
        }
    }
}
