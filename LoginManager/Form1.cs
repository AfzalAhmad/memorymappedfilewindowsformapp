﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MemoryMapped.Library;

namespace LoginManager
{
    public partial class Form1 : Form
    {
        bool loginStatus = false;       
        SharedMemory<MemoryMapped.Library.Message> shmem = null;

        public Form1()
        {
            InitializeComponent();
            lblStatus.BackColor = loginStatus ? Color.Green:Color.Red;
            
            lblStatus.Text = loginStatus? "loggged In" : "Logged Out";

            shmem = new SharedMemory<MemoryMapped.Library.Message>("ShmemTest", 32);
            shmem.Open();
            Write();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loginStatus = loginStatus ? false : true;

            lblStatus.BackColor = loginStatus ? Color.Green : Color.Red;

            lblStatus.Text = loginStatus ? "loggged In" : "Logged Out";
            
            Write();
           
        }

       

        private void Write()
        {           
            
            MemoryMapped.Library.Message data = new MemoryMapped.Library.Message();
            data.loggedIn = loginStatus;
            shmem.Data = data;
                    
        }

        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Close shared memory   
            shmem.Close();
        }
    }
}
