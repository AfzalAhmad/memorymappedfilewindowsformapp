﻿using MemoryMapped.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp
{
    public partial class ClientApp : Form
    {
        SharedMemory<MemoryMapped.Library.Message> shmem = null;
        public ClientApp()
        {
            InitializeComponent();
            shmem = new SharedMemory<MemoryMapped.Library.Message>("ShmemTest", 32);
            shmem.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var message = read();
            if (message.loggedIn)
            {
                MessageBox.Show("Status: LoggedIn", "Logged in Status");
            }
            else 
            {
                MessageBox.Show("Status: Logged Out","Logged in Status");
            }
        }

        

        private MemoryMapped.Library.Message read()
        {
            
            return shmem.Data;
           

           
        }

        private void ClientApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Close shared memory
            shmem.Close();
        }
    }
}
